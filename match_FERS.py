#!/usr/bin/env python3
###############################################################################
#                                                                             #
#   File and Version Information:                                             #
#       match_FERS.py, v.1.0, 15/6/2023                                       #
#                                                                             #
#   Description:                                                              #
#     perform inter-FERS matching of the ENUBET 2023 TB Janus data            #
#                                                                             #
#   Author List:                                                              #
#      Luca Bomben            lbomben@studenti.uninsubria.it                  #
#                                                                             #
###############################################################################



#############################
########## imports ##########
#############################

import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt


#####################################
########## parse arguments ##########
#####################################

parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="Binary input file name")
parser.add_argument("-b", "--nboards", help="Number of FERS boards (default = 2)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_npz/<sameasinput>_FERS_match.npz)")
parser.add_argument("-d", "--debug", help="Debug mode (additional diagnostic plots): 1 is ON, 0 is OFF")

args = parser.parse_args()
input_file_name = args.inputfile
outfile_name = args.output

#if not specified, assume there are 2 FERS
if not args.nboards:
    nboards=int(2)
else:
    nboards = int(args.nboards)

#if nost specified, use default output name
if not args.output:
    outfile_name = "FERS_npz/" + input_file_name.split("/")[-1][:-4] + "_FERS_match.npz"

#if not specified, debug mode is off
if not args.debug:
    debug_mode=0
else:
    debug_mode=int(args.debug)



#################################################################
########## doubles removal (for spurious double trigs) ##########
#################################################################

def remove_doubles(data,th):
    
    for i in range(nboards):
        dt=np.diff(data[i]["trigger_time_stamp"])
        doubles_loc=np.where(dt<th)
        doubles_loc=np.array(doubles_loc,dtype=int)
        #data[i]=np.delete(data[i],np.concatenate( [doubles_loc,doubles_loc+1] ))
        data[i]=np.delete(data[i],doubles_loc+1)
        
    return data
        

###################################################################
########## inter-FERS event matching, based on timestamp ##########
###################################################################

def match_FERS(data,th):
    
    lmin=np.min(np.array([ len(d) for d in data]))
    
    #timestamp arrays
    ts=[ d["trigger_time_stamp"] for d in data ]
    
    #deltat between coreesponding array elements, wrt board 0
    dt=[ ts[0][0:lmin] - ts[i][0:lmin] for i in range(1,nboards) ]
    
    #out-of-sync tagger: when |dt|>th
    oos=[ np.abs(d)>th for d in dt ]
    
    #count out-of-sync events per board
    n_oos=np.array([np.sum(o) for o in oos])
    
    while np.sum(n_oos)>0:
        
        #this returns the first board that has at least 1 oos event
        mybrd=np.argmax(n_oos>0)
        #this returns the first oos event
        myev=np.argmax(oos[mybrd])
        mydt=dt[mybrd][myev]
        
        #if dt>0 --> board 0 is ahead
        if(mydt>0):
            data[mybrd+1]=np.delete(data[mybrd+1],myev)

        #if dt<0 --> the other board is ahead
        if(mydt<0):
            data[0]=np.delete(data[0],myev)
        
        #re-compute relevant quantities
        lmin=np.min(np.array([ len(d) for d in data]))
        ts=[ d["trigger_time_stamp"] for d in data ]
        dt=[ ts[0][0:lmin] - ts[i][0:lmin] for i in range(1,nboards) ]
        oos=[ np.abs(d)>th for d in dt ]
        n_oos=np.array([np.sum(o) for o in oos])
    
    
    return [ d[0:lmin] for d in data ]


#############################################################
########## some formatting/printing/plotting utils ##########
#############################################################

def print_in_box(printstring,borderchar="#",nborder=2):
    
    lp=len(printstring)
    lb=len(borderchar)
    
    lline = lp + 2 + 2*nborder*lb
    deltal = lb - ( lline % lb )
    line2print = borderchar*nborder + " " + printstring + str(" ")*(deltal+1) + borderchar*nborder
    lborder= int( len(line2print)/lb )
    border = borderchar*lborder
    
    print(border)
    print(line2print)
    print(border)

def diag_plots(data):
    
    plt.figure(1)
    for i in range(1,nboards):
        plt.plot(data[i]["trigger_time_stamp"]-data[0]["trigger_time_stamp"],label=f"FERS {i} - FERS 0")
    plt.ylabel("$\Delta$t wrt FERS 0 [$\mu$s]")
    plt.xlabel("Trigger ID")
    plt.legend()
    plt.show()
    
    plt.figure(2)
    for i in range(1,nboards):
        plt.hist(data[i]["trigger_time_stamp"]-data[0]["trigger_time_stamp"],bins=100,label=f"FERS {i} - FERS 0")
    plt.ylabel("$\Delta$t wrt FERS 1 [$\mu$s]")
    plt.xlabel("Entries")
    plt.legend()
    plt.show()




###################################
########## start of main ##########
###################################


if __name__ == "__main__":
    
    #error if input doesn't exist
    if not os.path.isfile(input_file_name):
        print_in_box(f"Error: the file {input_file_name} does not exist!","!")
        sys.exit(1)
    
    
    #####################################################
    ########## open the file and read the data ##########
    #####################################################
    
    print_in_box(("match_FERS: opening file "+input_file_name),"#")
    
    #open the npz
    filein = np.load(input_file_name)
    
    #FERS data
    data=[ filein[f"arr_{i}"] for i in range(len(filein.files)-1) ]
    
    #dat file header
    head=filein["head"]
    
    #count events pre-alignment
    print_in_box("Before cleanup","=-=",1)
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i],"events in board",i)
    
    
    ##############################################
    ########## start cleanup & matching ##########
    ##############################################
    
    #remove doubles - threshold set to 500 us
    data=remove_doubles(data,500)
    
    #count events after doubles removal
    print_in_box("After doubles removal","=-=",1)
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i],"events in board",i)
    
    #perform inter-FERS matching - threshold set to 0.01 (usually more than enough)
    if nboards>1:
        data=match_FERS(data,0.01)
    
    #count event after everything is done
    print_in_box("After inter-FERS matching","=-=",1)
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i],"events in board",i)
        
    #some diagnostic plots
    if debug_mode:
        diag_plots(data)
    
    
    ##############################################
    ########## save the data in outfile ##########
    ##############################################
    
    np.savez(outfile_name,*data,head=head)
    print_in_box(("Data saved in "+outfile_name),"*")




