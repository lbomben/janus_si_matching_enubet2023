#!/usr/bin/env python3
###############################################################################
#                                                                             #
#   File and Version Information:                                             #
#       match_FERS_Si_syncpulses.py, v.1.0, 15/6/2023                         #
#                                                                             #
#   Description:                                                              #
#     match FERS data with Si DAQ data,                                       #
#     exploiting event timestamps & synchronization pulses                    #
#                                                                             #
#   Author List:                                                              #
#      Luca Bomben            lbomben@studenti.uninsubria.it                  #
#                                                                             #
###############################################################################




#############################
########## imports ##########
#############################

import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt
import re


#####################################
########## parse arguments ##########
#####################################

parser = argparse.ArgumentParser()
parser.add_argument("FERS_inputfile", help="FERS matched input file name (npz)")
parser.add_argument("Si_inputfile", help="Si DAQ timing input file name (root)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_Si_match/run_Si<nSi>_FERS_<nFERS>.txt)")
parser.add_argument("-d", "--debug", help="Debug mode (additional diagnostic plots): 1 is ON, 0 is OFF")

args = parser.parse_args()
FERS_input_file_name = args.FERS_inputfile
Si_input_file_name = args.Si_inputfile
outfile_name = args.output

#if nost specified, use default output name
if not args.output:
    outfile_name = "FERS_Si_match/run_Si" + (Si_input_file_name.split("/run")[-1]).split(".")[0] + "_FERS" + (FERS_input_file_name.split("/Run")[-1]).split("_list")[0] + ".txt"

#if not specified, debug mode is off
if not args.debug:
    debug_mode=0
else:
    debug_mode=int(args.debug)


#########################################################################
########## Si timestamp alignment: shouldn.t be needed anymore ##########
#########################################################################

def linearize_Si(ts_Si):
    
    dt=np.diff(ts_Si)
    
    jumps=np.array(np.where(dt<-10000),dtype=int)[0]
    
    if len(jumps)>0:
        for i in jumps:
            j=int(i+1)
            ts_Si[j:]=(ts_Si[j:]+(2**31))
    
    return ts_Si


#################################################################
########## FERS-Si timestamp matching: based on deltat ##########
#################################################################

def match_events(tss,tsf):
    
    final_match=[]
    
    #id & ts are already divided by spill
    #let's assume the Si detectors have the correct # of spills: no reason to assume otherwise
    for i in range(len(tss)):
    #for i in range(3):
        #shift the timestamps: origin is in the last sync pulse!
        tss_i=tss[i]-tss[i][-1]
        tsf_i=tsf[i]-tsf[i][-1]
        #simply associate each FERS event to the Si event with the closest (shifted) timestamp
        idmatch=[ np.argmin(np.abs(tss_i-tsf_i[j])) for j in range(len(tsf_i)) ]
        final_match.append(np.array(idmatch))
    
    return final_match
    
    


#######################################
########## tag 5 sync pulses ##########
#######################################

def tag_sync_pulses(ev_ts):
    
    #sync pulses should be ~ [1,2,3,4,5] * 135k us
    seq = np.array([1,2,3,4,5])
    
    #compute deltat
    dt=np.diff(ev_ts)
    #divide deltat by 135k and approximate to the closest integer (test runs showed that the uncertainty was waaay below 135k)
    arr = np.round( dt / 135000 )
    
    # Store sizes of input array and sequence
    Na, Nseq = arr.size, seq.size
    # Range of sequence
    r_seq = np.arange(Nseq)

    # Create a 2D array of sliding indices across the entire length of input array.
    # Match up with the input sequence & get the matching starting indices.
    M = (arr[np.arange(Na-Nseq+1)[:,None] + r_seq] == seq).all(1)
    
    # Get the range of those indices as final output
    if M.any() >0:
        return np.where( M>0 )[0]
    else:
        return []         # No match found
            
            
#############################################################
########## some formatting/printing/plotting utils ##########
#############################################################

def print_in_box(printstring,borderchar="#",nborder=2):
    
    lp=len(printstring)
    lb=len(borderchar)
    
    lline = lp + 2 + 2*nborder*lb
    deltal = lb - ( lline % lb )
    line2print = borderchar*nborder + " " + printstring + str(" ")*(deltal+1) + borderchar*nborder
    lborder= int( len(line2print)/lb )
    border = borderchar*lborder
    
    print(border)
    print(line2print)
    print(border)
            
def diag_plots(Si_ts,FERS_ts):
    
    plt.figure(1)
    plt.plot((Si_ts),"-*",label="Si")
    plt.plot((FERS_ts),"-*",label="FERS")
    plt.xlabel("Trigger ID")
    plt.ylabel("Timestamp")
    plt.legend()
    plt.show()
    
    plt.figure(2)
    plt.plot(np.diff(Si_ts),"-*",label="Si")
    plt.plot(np.diff(FERS_ts),"-*",label="FERS")
    plt.xlabel("Trigger ID")
    plt.ylabel("Inter-event $\Delta$t [$\mu$s]")
    plt.legend()
    plt.show()

def diag_plots_spill_tag(pulse_start_Si,pulse_start_FERS,spill_start_Si):
    
    plt.figure(3)
    plt.plot(pulse_start_Si+6,"-*",label="from sync pulse - Si")
    plt.plot(pulse_start_FERS+6,"-*",label="from sync pulse - FERS")
    plt.plot(spill_start_Si,"-*",label="from data - Si")
    plt.xlabel("Spill ID")
    plt.ylabel("Spill start")
    plt.legend()
    plt.show()
    
    
def diag_plots_spill_split(lspill_Si,lspill_FERS):
    
    plt.figure(4)
    plt.plot(lspill_Si,"-*",label="Si")
    plt.plot(lspill_FERS,"-*",label="FERS")
    plt.xlabel("Spill ID")
    plt.ylabel("Events per spill")
    plt.legend()
    plt.show()
    
    
def diag_plots_post_matching(Si_ts,FERS_ts,ts_cleanup,pulse_start_FERS):
    
    cond_cleanup=(np.abs(Si_ts_matched_shifted-FERS_ts_matched_shifted)>th_cleanup)
    id_cleanup=np.where(cond_cleanup)
    id_bad=np.where(1-cond_cleanup)
    
    plt.figure(5)
    plt.plot((Si_ts),"-*",label="Si (FERS-matched)")
    plt.plot((FERS_ts),"-*",label="FERS")
    plt.xlabel("FERS trigger ID")
    plt.ylabel("Timestamp (pre-cleanup)")
    plt.legend()
    plt.show()
    
    plt.figure(6)
    plt.plot(np.diff(Si_ts),"-*",label="Si (FERS-matched)")
    plt.plot(np.diff(FERS_ts),"-*",label="FERS")
    plt.xlabel("FERS Trigger ID")
    plt.ylabel("Inter-event $\Delta$t [$\mu$s]")
    plt.legend()
    plt.show()
    
    plt.figure(7)
    plt.hist((Si_ts_matched_shifted-FERS_ts_matched_shifted))
    plt.xlabel("Post-matching $\Delta$t [$\mu$s]")
    plt.show()
    
    plt.figure(8)
    plt.hist((Si_ts_matched_shifted-FERS_ts_matched_shifted),bins=100,range=[-200,200])
    plt.xlabel("Post-matching $\Delta$t (zoom) [$\mu$s]")
    plt.show()
    
    myx=np.arange(len(FERS_ts))
    
    plt.figure(9)
    plt.plot(id_cleanup[0],"-*",label="Events to be deleted")
    plt.xlabel("FERS Trigger ID")
    plt.legend()
    plt.show()
    
    Si_ts_clean=np.copy(Si_ts)
    FERS_ts_clean=np.copy(FERS_ts)
    Si_ts_trash=np.copy(Si_ts)
    FERS_ts_trash=np.copy(FERS_ts)
    
    
    Si_ts_clean=np.delete(Si_ts_clean,id_cleanup)
    FERS_ts_clean=np.delete(FERS_ts_clean,id_cleanup)
    Si_ts_trash=np.delete(Si_ts_trash,id_bad)
    FERS_ts_trash=np.delete(FERS_ts_trash,id_bad)
    FERS_dt_trash_pre=np.delete(np.diff(FERS_ts_trash),id_bad[:-1])
    FERS_dt_trash_post=np.delete(np.diff(FERS_ts_trash)[1:],id_bad[:-2])
    
    plt.figure(10)
    plt.plot((Si_ts_clean),"-*",label="Si (FERS-matched)")
    plt.plot((FERS_ts_clean),"-*",label="FERS")
    plt.xlabel("FERS trigger ID")
    plt.ylabel("Timestamp (post-cleanup)")
    plt.legend()
    plt.show()
    
    plt.figure(11)
    plt.plot((Si_ts_trash),"-*",label="Si (FERS-matched)")
    plt.plot((FERS_ts_trash),"-*",label="FERS")
    plt.xlabel("FERS trigger ID")
    plt.ylabel("Timestamp (deleted events)")
    plt.legend()
    plt.show()
    
    plt.figure(12)
    plt.plot((FERS_dt_trash_pre),"-*",label="from previous event")
    plt.plot((FERS_ts_trash),"-*",label="to following event")
    plt.xlabel("FERS trigger ID")
    plt.ylabel("Delta t (deleted events) [$\mu$s]")
    plt.legend()
    plt.show()
    
    
    
    

###################################
########## start of main ##########
###################################


if __name__ == "__main__":
    
    
    print_in_box(("match_FERS_Si_syncpulses: opening files "+FERS_input_file_name+" and "+Si_input_file_name),"#")
    if not os.path.isfile(FERS_input_file_name):
        print_in_box(f"Error: the file {FERS_input_file_name} does not exist!","!")
        sys.exit(1)
    
    if not os.path.isfile(Si_input_file_name):
        print_in_box(f"Error: the file {Si_input_file_name} does not exist!","!")
        sys.exit(1)
        
    ##########################################################
    ########## open the FERS file and read the data ##########
    ##########################################################
    
    #open the npz
    FERS_in = np.load(FERS_input_file_name)
    data_FERS=[ FERS_in[f"arr_{i}"] for i in range(len(FERS_in.files)-1) ]
    head_FERS=FERS_in["head"]
    FERS_ts=np.array(np.copy(data_FERS[0]["trigger_time_stamp"]))
    FERS_id=np.array(np.copy(data_FERS[0]["trigger_id"]))
    
    #start of acquisition (in us?)
    acqstart_FERS=(head_FERS["acq_start"][0])
    
    
    ############################################################
    ########## open the Si DAQ file and read the data ##########
    ############################################################
    
    with uproot.open(Si_input_file_name) as Si_in:
        tree=Si_in["t"]
        Si_ts=tree["timestamp"].array(library="np")
        Si_id=tree["trig_ID"].array(library="np")
    
    #move origin to 0
    Si_ts=Si_ts-Si_ts[0]
    Si_ts=Si_ts.astype(np.int32)
    
    #print event numbers at start
    print_in_box("Pre-matching","=-=",1)
    print("Si detectors:",len(Si_id),"events")
    print("FERS:",len(FERS_id),"events")
    
    #some diagnostic plots
    if debug_mode:
        diag_plots(Si_ts,FERS_ts)
    
    
    ####################################
    ########## tag the spills ##########
    ####################################
        
    pulse_start_Si=tag_sync_pulses(Si_ts)
    pulse_start_FERS=tag_sync_pulses(FERS_ts)
    nspill_Si=len(pulse_start_Si)
    nspill_FERS=len(pulse_start_FERS)
    print_in_box("Spill search","=-=",1)
    print("Found",nspill_Si,"sync sequences in the Si detectors")
    print("Found",nspill_FERS,"sync sequences in the FERS")
    spill_start_Si=(np.where(Si_id==0)[0])[1:]
    print("Found",len(spill_start_Si),"spill starts in the Si detectors")
    
    #some diagnostic plots
    if debug_mode:
        diag_plots_spill_tag(pulse_start_Si,pulse_start_FERS,spill_start_Si)
        
    
    ##############################################
    ########## split the data in spills ##########
    ##############################################
    
    Si_ts_spilled=np.split(Si_ts,spill_start_Si)
    Si_id_spilled=np.split(Si_id,spill_start_Si)
    
    FERS_ts_spilled=np.split(FERS_ts,pulse_start_FERS+6)
    FERS_id_spilled=np.split(FERS_id,pulse_start_FERS+6)
    
    lspill_Si=[len(v) for v in Si_id_spilled ]
    lspill_FERS=[len(v) for v in FERS_id_spilled ]
    
    #some diagnostic plots
    if debug_mode:
        diag_plots_spill_split(lspill_Si,lspill_FERS)
    
    
    ####################################################
    ########## match the events within spills ##########
    ####################################################
    
    idmatch = match_events(Si_ts_spilled,FERS_ts_spilled)
    
    
    #############################################
    ########## clean-up spurious trigs ##########
    #############################################
    
    Si_ts_spilled_matched=[]
    Si_ts_spilled_matched_shifted=[]
    FERS_ts_spilled_matched_shifted=[]
    Si_id_spilled_matched=[]
    FERS_id_spilled_matched=[]
    for i in range(len(idmatch)):
        Si_ts_spilled_matched.append( np.array( [ Si_ts_spilled[i][ idmatch[i][j] ] for j in range(len(idmatch[i])-5) ] ) ) 
        Si_id_spilled_matched.append( np.array( [ Si_id_spilled[i][ idmatch[i][j] ] + np.cumsum(np.concatenate([[0],lspill_Si[:-1]]))[i] for j in range(len(idmatch[i])-5) ] ) ) 
        Si_ts_spilled_matched_shifted.append( np.array( [ Si_ts_spilled[i][ idmatch[i][j] ] - Si_ts_spilled[i][-1] for j in range(len(idmatch[i])-5) ] ) ) 
        FERS_ts_spilled_matched_shifted.append( np.array( FERS_ts_spilled[i][0:-5]-FERS_ts_spilled[i][-1] ) ) 
        FERS_id_spilled_matched.append( np.array( FERS_id_spilled[i][0:-5] ) ) 
   
   
    Si_id_matched=np.concatenate( Si_id_spilled_matched )
    FERS_id_matched=np.concatenate( FERS_id_spilled_matched )
    FERS_ts_matched_shifted=np.concatenate( FERS_ts_spilled_matched_shifted )
    Si_ts_matched_shifted=np.concatenate( Si_ts_spilled_matched_shifted )
    
    #delete events where the discrepancy is > 200 us
    th_cleanup=200
    id_cleanup=np.where(np.abs(Si_ts_matched_shifted-FERS_ts_matched_shifted)>th_cleanup)
    
    Si_id_clean=np.delete(Si_id_matched,id_cleanup)
    FERS_id_clean=np.delete(FERS_id_matched,id_cleanup)
    
    
    #print event numbers after cleanup
    print_in_box("Post matching","=-=",1)
    print("Si detectors:",len(Si_id_clean),"events")
    print("FERS:",len(FERS_id_clean),"events")
    print(f"Lost events in Si: {len(Si_id)-len(Si_id_clean)}, of which {nspill_Si*5} are sync pulses, {len(Si_id)-len(Si_id_clean)-nspill_Si*5} are not")
    print(f"Lost events in FERS: {len(FERS_id)-len(FERS_id_clean)}, of which {nspill_FERS*5} are sync pulses, {len(FERS_id)-len(FERS_id_clean)-nspill_FERS*5} are not")
    
    #some diagnostic plots
    if debug_mode:
        diag_plots_post_matching(Si_ts_matched_shifted,FERS_ts_matched_shifted,th_cleanup,pulse_start_FERS)
    
    
    ##############################################
    ########## save the data in outfile ##########
    ##############################################
    
    #when associating later, careful about the stripping! The current event id rolls over at each spill...
    with open(outfile_name, 'w') as fout:
        
        for i in range(len(Si_id_clean)):
            str2print=f"{Si_id_clean[i]} {FERS_id_clean[i]}\n"
            fout.write(str2print)

    print_in_box(("Data saved in "+outfile_name),"*")
    
    
    
    
    
