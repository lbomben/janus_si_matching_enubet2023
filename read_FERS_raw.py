#!/usr/bin/env python3
###############################################################################
#                                                                             #
#   File and Version Information:                                             #
#       read_FERS_raw.py, v.1.0, 15/6/2023                                    #
#                                                                             #
#   Description:                                                              #
#     read the FERS raw data from Janus binary files, convert it to npz.      #
#     Developed starting from Janus2root.py by V. Mascagna (UniBS & INFN-PV)  #
#                                                                             #
#   Author List:                                                              #
#      Luca Bomben            lbomben@studenti.uninsubria.it                  #
#                                                                             #
###############################################################################



#############################
########## imports ##########
#############################

import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt


#####################################
########## parse arguments ##########
#####################################

parser = argparse.ArgumentParser()

parser.add_argument("inputfile", help="Binary input file name")
parser.add_argument("-b", "--nboards", help="Number of FERS boards (default = 2)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_npz/<sameasinput>_raw.npz)")
parser.add_argument("-m", "--mask", help="Hex channel mask (default = 0xFFFF,0xFFFF for every board)")
parser.add_argument("-d", "--debug", help="Debug mode (additional diagnostic plots): 1 is ON, 0 is OFF")

args = parser.parse_args()
input_file_name = args.inputfile
outfile_name = args.output

#if not specified, assume there are 2 FERS
if not args.nboards:
    nboards=int(2)
else:
    nboards = int(args.nboards)
    
#if nost specified, use default output name
if not args.output:
    outfile_name = "FERS_npz/" + input_file_name.split("/")[-1][:-4] + "_raw.npz"

#if not specified, debug mode is off
if not args.debug:
    debug_mode=0
else:
    debug_mode=int(args.debug)

#if nor specified, assume all channels are saved
if not args.mask:
    chmask=[hex(0xFFFF)]*nboards*2
else:
    chmask=str(args.mask).split(",")
    chmask=[hex(int(w,16)) for w in chmask]
#convert mask to binary
binmask=[ bin(int(w,16))[2:].zfill(32) for w in chmask ]
#count channels for each FERS
nchs=[ b.count("1") for b in binmask ]
nchs = [ nchs[i]+nchs[i+1] for i in range(nboards) ]
#op_mode 0 = "arbitrary nr. of channels" --> use complex data structure --> slower readout
#op_mode 1 = "same nr. of channels for all" --> use simpler data structure --> faster readout
op_mode=0;
if ( nchs.count(nchs[0]) == len(nchs) ):
    op_mode=1;
    nchs=nchs[0];
print("Channels per FERS =",nchs)
        


####################################
########## data structure ##########
####################################

# File header according to the Janus software manual pag 38
#
# Binary file structure memo:
# 
# [FILE HEADER] +
#   nevents x 
#       nboards x 
#            [EVENT HEADER] + 
#            nchannels[board] x [DATA]


# File header: common to all acquisition modes:
Janus_file_header = np.dtype(
    [
        ("data_format_version", ("B", 2)),  # 16 bits, Data Format Version
        ("software_version", ("B", 3)),  # 24 bits, Data Format Version
        ("acq_mode", np.dtype("B")),  # 8  bits, Acq Mode
        ("time_unit", np.dtype("B")),  # 8  bits, Time Unit # new v3
        ("energy_n_channels",np.dtype(">u2")),  # 16  bits, Energy NChannels # new v3
        ("time_conversion", np.dtype("<f4")), # 32 bits, Time Conversion # new v3
        ("acq_start", np.dtype("u8"))  # 64 bits, Start Acquisition
    ]
)

# Acquisition modes dict (just to print them).... in reality, we only use spectroscopy
acq_modes = {
    1: "Spectroscopy mode",
    2: "Timing Mode",
    3: "Spectroscopy + Timing Mode",
    4: "Counting Mode"
}

# the event schema to be repeated nch times
Janus_event_data_schema = {
    1: np.dtype(  # data for event in spectroscopy mode
        [
            ("channel_id", np.dtype("B")),
            ("data_type", np.dtype("B")),
            ("lg_pha", np.int16),
            ("hg_pha", np.int16),
        ]
    )
}


# now the general event schema

#complex case: arbitrary nr. of channels per FERS --> split the event in "header" (timestamp, event id...) & "ph" (actual data)
#define a different data structure for each baord, so that it can have a different nr. of channels
#finally, define a full "event" structure where all the info can be put after reading it
if op_mode==0:
    Janus_event_header_schema = {
        1: np.dtype(
            [
                ("event_size", np.dtype("u2")),
                ("board_id", np.dtype("B")),
                ("trigger_time_stamp", np.dtype("f8")),
                ("trigger_id", np.dtype("u8")),
                ("channel_mask", np.dtype("u8"))
            ]
        )
    }
    Janus_event_ph_schema = {}
    Janus_event_schema = {}
    for i in range(nboards):       
        Janus_event_ph_schema[(1,i)]=np.dtype( [ ("ch_data", (Janus_event_data_schema[1], nchs[i])) ] )
        Janus_event_schema[(1,i)]=np.dtype(
                [
                    ("event_size", np.dtype("u2")),
                    ("board_id", np.dtype("B")),
                    ("trigger_time_stamp", np.dtype("f8")),
                    ("trigger_id", np.dtype("u8")),
                    ("channel_mask", np.dtype("u8")),
                    ("ch_data", (Janus_event_data_schema[1], nchs[i]))
                ]
            )
    
#simpler case: same nr. channel for each FERS
if op_mode==1:
    
    Janus_event_schema = {
        1: np.dtype(
            [
                ("event_size", np.dtype("u2")),
                ("board_id", np.dtype("B")),
                ("trigger_time_stamp", np.dtype("f8")),
                ("trigger_id", np.dtype("u8")),
                ("channel_mask", np.dtype("u8")),
                ("ch_data", (Janus_event_data_schema[1], nchs))
            ]
        )
    }
    


####################################################
########## read single event in op_mode 0 ##########
####################################################

def read_single_event(fp):
    
    #read event header --> used to find board ID
    ev_head=np.fromfile(fp, dtype=Janus_event_header_schema[1], count=1)
    
    #ev_head is empty <=> reached eof --> return an arbitrary output
    if len(ev_head)==0:
        return np.array([0],dtype=np.int16) 
    
    #read pulse heights
    ev_ph=np.fromfile(fp, dtype=Janus_event_ph_schema[(1,ev_head["board_id"][0])], count=1)
    
    #put the data in a single Janus_event_schema array element
    ev_tot=np.zeros(1,dtype=Janus_event_schema[(1,ev_head["board_id"][0])])
    ev_tot["event_size"]=ev_head["event_size"]
    ev_tot["board_id"]=ev_head["board_id"]
    ev_tot["trigger_time_stamp"]=ev_head["trigger_time_stamp"]
    ev_tot["trigger_id"]=ev_head["trigger_id"]
    ev_tot["ch_data"]=ev_ph["ch_data"]
    
    return ev_tot

    
#############################################################
########## some formatting/printing/plotting utils ##########
#############################################################

def better_print(byte_seq):
    '''Just a nice representation of
    arrays of 1 byte integers (file header)
    Example "1.1" instead of "array[1,1]"'''

    return ".".join([str(n) for n in byte_seq])

def print_in_box(printstring,borderchar="#",nborder=2):
    
    lp=len(printstring)
    lb=len(borderchar)
    
    lline = lp + 2 + 2*nborder*lb
    deltal = lb - ( lline % lb )
    line2print = borderchar*nborder + " " + printstring + str(" ")*(deltal+1) + borderchar*nborder
    lborder= int( len(line2print)/lb )
    border = borderchar*lborder
    
    print(border)
    print(line2print)
    print(border)
    

def print_file_info(file_header_data):
    print("Data Format Version: " + better_print(file_header_data["data_format_version"][0]))
    print("Software Version: " + better_print(file_header_data["software_version"][0]))
    print("Acquisition Mode: " + str(file_header_data["acq_mode"][0]))
    print("Time Unit: " + str(file_header_data["time_unit"][0]))
    print("Energy N Channels: " + str(file_header_data["energy_n_channels"][0]) + " bins")
    print("Time Conversion: " + str(file_header_data["time_conversion"][0]) + " ns/LSB")

def diag_plots(data):
    #some diagnostic plots
    
    nev=[ len(v) for v in data ]
    trigid=[ v["trigger_id"] for v in data ]
    tstamp=[ v["trigger_time_stamp"] for v in data ]
    lmin=np.min(np.array(nev))
    
    plt.figure(1)
    for i in range(nboards):
        plt.plot(tstamp[i],"-*",label=f"FERS {i}")
    plt.xlabel("Event ID")
    plt.ylabel("Timestamp [$\mu$s]")
    plt.legend()
    plt.show()
        
    plt.figure(2)
    for i in range(nboards):
        plt.plot(np.diff(tstamp[i]),"-*",label=f"FERS {i}")
    plt.xlabel("Event ID")
    plt.ylabel("$\Delta$t [$\mu$s]")
    plt.legend()
    plt.show()
        
    
    plt.figure(3)
    for i in range(1,nboards):
        plt.plot(tstamp[i][0:lmin]-tstamp[0][0:lmin],"-*",label=f"FERS {i} - FERS 0")
    plt.xlabel("Event ID")
    plt.ylabel("$\Delta$t (inter-FERS) [$\mu$s]")
    plt.legend()
    plt.show()




###################################
########## start of main ##########
###################################


if __name__ == "__main__":

    #error if input doesn't exist
    if not os.path.isfile(input_file_name):
        print_in_box(f"Error: the file {input_file_name} does not exist!","!")
        sys.exit(1)

    #####################################################
    ########## open the file and read the data ##########
    #####################################################
    
    print_in_box(("read_FERS_raw: opening file "+input_file_name),"#")
    
    
    with open(input_file_name, "rb") as fp:

        # file header, 1 time
        head = np.fromfile(fp, dtype=Janus_file_header, count=1)
        print_file_info(head)
        acq_mode = head["acq_mode"][0]
        
        if op_mode == 0:
            data=[ np.empty(1, dtype=Janus_event_schema[(acq_mode,i)]) for i in range(nboards) ]
            
            while True:
                a=read_single_event(fp)
                
                # if eof, break
                if a.dtype==np.int16:
                    break;
                nb=int(a["board_id"][0])
                data[nb]=np.append(data[nb],a)
        
        #op_mode 1 --> one single np.fromfile (count=-1 means keep going until eof)
        if op_mode == 1:
            data_mix = np.fromfile(fp, dtype=Janus_event_schema[acq_mode], count=-1)
            data=[ data_mix[data_mix["board_id"]==nb] for nb in range(nboards) ]
        
    #count the events
    print_in_box("Events read","=-=",1)
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i],"in board",i)
    
    
    #some diagnostic plots
    if debug_mode:
        diag_plots(data)
        
        
    
    ##############################################
    ########## save the data in outfile ##########
    ##############################################
    
    np.savez(outfile_name,*data,head=head)
    print_in_box(("Data saved in "+outfile_name),"*")
        
        
    
    

































