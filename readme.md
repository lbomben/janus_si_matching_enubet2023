## Analysis utilities for the 2022 beam test data

To run all of the code, you will need:

* python 3 and hadd for tree manipulation
 
* root 6 for the .C codes

* mupdf to run do_MIP_1by1.sh




### analysis_utils/tree_manip:

python utilities for manipulation/merging of run trees.

* ` python3 merge_mip_scan.py `

  merge the trees for the MIP scan, shifting the coordinates and adding the "runNumberFERS" and "runNumberSi" branches.

  Settings are in the mip_scan_2022.cfg file

* `python3 merge_energy_scan.py`
  
  merge the trees for the energy scan, shifting the coordinates and adding the "beamEnergy", "runNumberFERS" and "runNumberSi" branches.
  
  Settings are in the energy_scan_2022.cfg file

* `python3 add_gain_setting_branch.py <input> <output>`
  
  adds the "LG" and "HG" branches to the scan files, where HG and LG are the Janus gain settings.

  takes file input.root from the input directory, saves output.root in the output directory.
  
  Input/output dirs are set in the code.

* `python3 replace_FERS_variable.py <input> <output>`

  replaces "fersLG[8][64]" with 8 "fersLG_X[64]" branches (same with fersHG) as it seems to speed up root.

  Still to be verified how effective it actually is.

  takes file input.root from the input directory, saves output.root in the output directory.

  Input/output dirs are set in the code.

* `python3 make_tree_subset.py`

  make a subset of a certain tree, according to a selection set within the code (currently: trigger on the mu catcher)

* `./prepare_mip_scan_file.sh`

  prepare run090002.root, with the MIP scan data. Also applies add_gain_setting_branch.py and replace_FERS_variable.py

* `./prepare_energy_scan_file.sh`

  prepare run090003.root, with the energy scan data. Also applies add_gain_setting_branch.py and replace_FERS_variable.py

      











      
### analysis_utils/enubet_tb22_ana_utils:

root code with some useful pre-packaged utilities, including:
    
* automatically loading various parameters (MIP calibrations, channel map, SiBC alignment...) from the conf_files directory
      
  parameters are saved in suitable variables (defined in the parameters_definition.h file)

* defining various aliases for a TTree that are useful for the analysis, such as MIP-calibrated pulse heights, projected coordinates, total pulse heights...
  
Aliases are defined in the tree_preparation.h file.
  
Relevant paths are defined in the directories_definition.h file.
    
To include in your root code, at the start of the code use:
    
`#include "./enubet_tb22_ana_utils/enubet_tb22_ana_utils.h"`
    
Then, within the main:
    
`read_conf();` to load all parameters

`prepare_tree(t);` to add all of the useful aliases to an existing TTree with pointer t.
  










    

### root code to perform various tasks:

***Important***: all code requires the presence of the enubet_tb22_ana_utils directory and its contents. As such, the directory structure specified in enubet_tb22_ana_utils/directories_definition.h is followed.

* `root -l "align_div.C+(nrun,wri)"`
      
  perform alignment of the Si trackers based on run nrun (nrun is the Si run number, defautl=189).
  
  ***Important***: check that nrun doesn't correspond to tilted beam!
      
  Currently only using runs contained within run090002 (the MIP scan).
  
  If wri=1 (default), alignment parameters are saved in conf_files/angcal/run090002_Si\<nrun\>_angcal.txt

* `root -l "corr_gain.C+(nb,nc,wri,show)"`
  
  compute LG setting correction for FERS nr. nb, channel nr. nc.
  
  If wri=1 (default), correction parameters are saved in conf_files/gaincal/run090002_b\<nb\>_c\<nc\>_gaincal.txt and histograms/fit functions are saved in root_out/HGvLG/run090002_b\<nb\>_c\<nc\>_HGvLG.root
  
  If show=1 (default), plots are shown.

* `./all_corr_gain.sh`

  run corr_gain.C on all channels.

* `root -l "plot_MIP.C+(nb,nc,plot_cond,wri,show,save_pdf)"`
      
  compute and fit the MIP spectrum for FERS nr. nb, channel nr. nc. If nb=8, digitizer channels are used instead.
  
  For FERS T0 channels, high gain is used. For T1,T2,T3, low gain is used.
  
  A cut defined by plot_cond is applied to all data. If plot_cond="fidcut" the suitable fiducial cut is applied. Otherwise, any combination of tree branches and aliases (defined in enubet_tb22_ana_utils/tree_preparation.h) can be used.
      
  If wri=1 (default), histograms/fit functions are saved in root_out/MIP/run090002_b\<nb\>_c\<nc\>_MIP.root
  
  If show=1 (default), plots are shown.
  
  If save_pdf=1, plots are saved in plots/run090002_b\<nb\>_c\<nc\>_MIP.pdf

* `./all_MIP.sh`
  
  run plot_MIP.C for all channels.
    
* `./do_MIP_1by1.sh`
  
  interactively run plot_MIP.C on all channels, one by one, in order to check and correct fit results.

* `./show_all_MIP.sh`
  
  run show_all_MIP.C to show the results of MIP fits for all the channels and save all MIP calibration coeffs in conf_files/MIPcal/run090002_MIPcal.txt.
  
  ***Important***: must be run in order to update the MIP calib. coefficients used by enubet_tb22_ana_utils
    
* `root -l "find_fidcut.C+(nb,nc,wri,show,save_pdf)"`
  
  Compute efficiency plot and try to find a fiducial cut from it (not very reliable) for FERS nr. nb, channel nr. nc. If nb=8, digitizer channels are used instead.
  
  If wri=1 (default), histograms are saved in root_out/eff/run090002_b\<nb\>_c\<nc\>_eff.root and fiducial cut center is saved in conf_files/fidcut/run090002_b\<nb\>_c\<nc\>_coords.txt
  
  If show=1 (default), plots are shown.
  
  If save_pdf=1, plots are saved in plots/run090002_b\<nb\>_c\<nc\>_eff.pdf
    
* `./all_fidcut.sh`
  
  run find_fidcut.C for all channels.

* `root -l "find_baseline.C+(nb,nc,plot_cond,wri,show,save_pdf)"`
  
  compute the baseline spectrum and find the baseline for FERS nr. nb, channel nr. nc. If nb=8, digitizer channels are used instead.
  
  For FERS T0 channels, high gain is used. For T1,T2,T3, low gain is used.
  
  A cut defined by plot_cond is applied to all data. If plot_cond="fidcut" the suitable fiducial cut is applied. Otherwise, any combination of tree branches and aliases (defined in enubet_tb22_ana_utils/tree_preparation.h) can be used.
  
  If wri=1 (default), baseline value is saved in conf_files/baseline/run090002_b\<nb\>_c\<nc\>_bl.txt
  
  If show=1 (default), plots are shown.
  
  If save_pdf=1, plots are saved in plots/run090002_b\<nb\>_c\<nc\>_bl.pdf

* `./all_baseline.sh`
  
  run find_baseline.C for all channels.

* `root -l "fit_ene_scan.C+"`
  
  Perform analysis of the energy scan data
      
