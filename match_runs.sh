###############################################################################
#                                                                             #
#   File and Version Information:                                             #
#       match_runs.sh, v.1.0, 15/6/2023                                       #
#                                                                             #
#   Description:                                                              #
#     pre-process & match the Si DAQ & Janus data for hte ENUBET 2023 TB      #
#                                                                             #
#   Author List:                                                              #
#      Luca Bomben            lbomben@studenti.uninsubria.it                  #
#                                                                             #
###############################################################################



#############################################
########## parse input run numbers ##########
#############################################

#Si run number
nrun_Si=$1;
echo "Si run number: ${nrun_Si}";

#FERS run number - if left empty, assume it's nrun_Si%10000
nrun_FERS=$2;
if [ "$nrun_FERS" == "" ] ; then 
    nrun_FERS=${nrun_Si: -4}; 
    for i in {1..5}; do
        nrun_FERS=${nrun_FERS#0};
    done
fi
echo "FERS run number: ${nrun_FERS}";

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
echo "%%%%%%%%%%                                  %%%%%%%%%%";
echo "%%%%%%%%%%   match_runs:                    %%%%%%%%%%";
echo "%%%%%%%%%%   Processing Si DAQ run ${nrun_Si}   %%%%%%%%%%";
echo "%%%%%%%%%%   Processing Janus run ${nrun_FERS}$(printf "\t")    %%%%%%%%%%";
echo "%%%%%%%%%%                                  %%%%%%%%%%";
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";



#################################
########## debug flags ##########
#################################


debug_raw=1;
debug_match_FERS=1;
debug_match_FERS_Si=1;



#####################################
########## set directories ##########
#####################################

#FERS data directory
dat_dir=FERS_raw;

#npz directory (for intermediate files)
npz_dir=FERS_npz;

#root directory (for DAQ root timing files)
root_dir=Si_raw;

#matched files directory
match_dir=FERS_Si_match;

#final ascii files directory
ascii_final_dir=ascii_merged;



##########################################################
########## extract data from the FERS info file ##########
##########################################################

FERS_info_name=${dat_dir}/Run${nrun_FERS}_Info.txt;

#number of FERS
nFERS=$(grep -c 'Open\[' ${FERS_info_name})
echo "Number of FERS: ${nFERS}";

#channel enable masks

#default mask
chmask0_default=$(grep "ChEnableMask0 " ${FERS_info_name});
chmask0_default=${chmask0_default#*0x}; chmask0_default=${chmask0_default% #*}; chmask0_default=$(echo ${chmask0_default} | sed -e 's/ //g');
chmask1_default=$(grep "ChEnableMask1 " ${FERS_info_name});
chmask1_default=${chmask1_default#*0x}; chmask1_default=${chmask1_default% #*}; chmask1_default=$(echo ${chmask1_default} | sed -e 's/ //g');
chmask_default="0x${chmask0_default},0x${chmask1_default}";
echo "Default FERS channel mask: $chmask_default";


chmask_full="";

#custom masks
for ((i=0; i<${nFERS}; i++))
do
    chmask0_custom=$(grep "ChEnableMask0\[${i}\]" ${FERS_info_name});
    chmask0_custom=${chmask0_custom#*0x}; chmask0_custom=${chmask0_custom%#*}; chmask0_custom=$(echo ${chmask0_custom} | sed -e 's/ //g');
    chmask1_custom=$(grep "ChEnableMask1\[${i}\]" ${FERS_info_name});
    chmask1_custom=${chmask1_custom#*0x}; chmask1_custom=${chmask1_custom%#*}; chmask1_custom=$(echo ${chmask1_custom} | sed -e 's/ //g');
    if [ "$chmask0_custom" == "" ] ; then chmask0_custom=$chmask0_default; fi
    if [ "$chmask1_custom" == "" ] ; then chmask1_custom=$chmask1_default; fi
    chmask_custom="0x${chmask0_custom},0x${chmask1_custom}";
    echo "FERS ${i} channel mask: $chmask_custom";
    if [ "$chmask_full" == "" ] ; 
    then 
        chmask_full="${chmask_custom}"; 
    else
        chmask_full="${chmask_full},${chmask_custom}";
    fi
done

echo "Full channel mask string is $chmask_full";



####################################################
########## prepare the Si timing rootfile ##########
####################################################

rootfile=${root_dir}/run${nrun_Si}_Si_timing.root;

if [ -f ${rootfile} ] ; then
#     echo "Merged Si root file already present --> moving on...";
    rm ${rootfile};
fi

#hadd in a single file
echo "Appending root files...";
hadd ${rootfile} ${root_dir}/run${nrun_Si}_*.root >> crap.txt;



##############################################################
########## prepare the Si post-stripping ASCII file ##########
##############################################################
# 
# Siasciifile=${Si_ascii_dir}/run${nrun_Si}.dat;
# 
# if [ -f ${Siasciifile} ] ; then
#     echo "Merged Si ascii file already present --> moving on...";
# else
#     ./merge_ascii_Si.sh ${nrun_Si} ${Si_ascii_dir};
# fi



########################################################
########## prepare the FERS raw data npz file ##########
########################################################

datfile=${dat_dir}/Run${nrun_FERS}_list.dat;
npzfile_raw=${npz_dir}/run${nrun_FERS}_raw.npz;

./read_FERS_raw.py -b ${nFERS} -m ${chmask_full}  -d ${debug_raw} -o ${npzfile_raw} ${datfile};



#######################################################
########## prepare the FERS-matched npz file ##########
#######################################################

npzfile_match=${npz_dir}/run${nrun_FERS}_FERS_match.npz;

./match_FERS.py -b ${nFERS} -o ${npzfile_match} -d ${debug_match_FERS} ${npzfile_raw};



####################################################################
########## prepare the FERS-Si event ID matching txt file ##########
####################################################################

matchfile=${match_dir}/run_Si${nrun_Si}_FERS${nrun_FERS}_match.txt;

./match_FERS_Si_syncpulses.py -o ${matchfile} -d ${debug_match_FERS_Si} ${npzfile_match} ${rootfile};



########################################################
########## write the post-matching ASCII file ##########
########################################################

# finalfile=${ascii_final_dir}/run_Si${nrun_Si}_FERS${nrun_FERS}_ascii.txt;
# 
# ./write_merged_ascii.py -o ${finalfile} ${matchfile} ${Siasciifile} ${chmap_final};



###################################
########## upload on eos ##########
###################################

# echo "Upload on eos? \"y\" = yes, else = no";
# read answer;
# if [ "$answer" == "y" ] ; then
#     scp ${finalfile} lbomben@lxplus.cern.ch:${eosdir};
# fi


